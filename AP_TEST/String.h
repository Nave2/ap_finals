#ifndef STRING_H
#define STRING_H

#include "type.h"

class String : public Type
{
private:
	std::string _value;
public:
	String(std::string val);
	bool isPrintable() const override;
	std::string toString() const override;
};

#endif // STRING_H