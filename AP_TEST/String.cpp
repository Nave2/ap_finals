#include "String.h"

String::String(std::string val)
{
	this->_isTemp = true;
	this->_value = val;
	this->_value[0] = '\'';
	this->_value[val.length() - 1] = '\'';
}

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	return this->_value;
}
