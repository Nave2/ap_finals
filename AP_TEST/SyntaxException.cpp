#include "SyntaxException.h"

const char* SyntaxException::what() const throw()
{
	return "SyntaxException: invalid syntax";
}