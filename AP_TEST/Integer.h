#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
private:
	int _value;
public:
	Integer(std::string val);
	bool isPrintable() const override;
	std::string toString() const override;
};

#endif // INTEGER_H