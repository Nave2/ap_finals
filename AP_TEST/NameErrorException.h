#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H

#include "InterperterException.h"
#include <string>
#include <sstream>

class NameErrorException : public InterperterException
{
private:
	std::string _name;
public:
	NameErrorException(std::string val);
	virtual const char* what() const throw();
};

#endif // NAME_ERROR_EXCEPTION_H