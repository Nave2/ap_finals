#include "type.h"

Type::Type()
{
	this->_isTemp = false;
}

bool Type::getIsTemp() const
{
	return this->_isTemp;
}

void Type::setIsTemp(bool value)
{
	this->_isTemp = value;
}
