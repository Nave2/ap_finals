#include "NameErrorException.h"
#include <iostream>
#pragma warning(disable : 4996)

NameErrorException::NameErrorException(std::string val)
{
	this->_name = val;
}

const char* NameErrorException::what() const throw()
{
	std::stringstream ss;
	ss << "NameError: name '" << this->_name << "' is not defined" ;
	std::string str = ss.str();
	char* c = new char[str.length()];
	strncpy(c, str.c_str(), str.length());
	c[str.length()] = 0;
	return c;
}