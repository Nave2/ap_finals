#include "Integer.h"

Integer::Integer(std::string val)
{
	this->_isTemp = true;
	this->_value = std::stoi(val);
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(this->_value);
}
