#include "Boolean.h"

Boolean::Boolean(std::string val)
{
	this->_isTemp = true;
	//if it reached here val must be "True" or "False"
	this->_value = (val == "True") ? true : false;
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return (this->_value) ? "True" : "False";
}
