#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type
{
private:
	bool _value;
public:
	Boolean(std::string val);
	bool isPrintable() const override;
	std::string toString() const override;
};

#endif // BOOLEAN_H