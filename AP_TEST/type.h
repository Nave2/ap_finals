#ifndef TYPE_H
#define TYPE_H

#include <string>

class Type
{
protected:
	bool _isTemp;
public:
	Type();
	bool getIsTemp() const;
	void setIsTemp(bool value);
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
};

#endif //TYPE_H
