#ifndef LIST_H
#define LIST_H

#include "Sequence.h"
#include <vector>

class List : public Sequence
{
private:
	std::vector<Type*> _items;
public:
	List(std::string val);
	bool isPrintable() const override;
	std::string toString() const override;
};

#endif // LIST_H