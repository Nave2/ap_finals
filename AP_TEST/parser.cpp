#include "parser.h"
#include <iostream>
#include <algorithm>
#include "Void.h"
#include "Boolean.h"
#include "NameErrorException.h"
#include "String.h"
#include "Integer.h"
#include <functional>
#pragma once

#define TAB 9

std::unordered_map<std::string, Type*> Parser::_variables;

void Parser::removeVariables()
{
	Parser::_variables.clear();
}

Type* Parser::parseString(std::string str) throw()
{
	if (str.length() > 0)
	{
		if (str[0] == TAB || str[0] == ' ')
			throw IndentationException();
		Helper::rtrim(str);

		if (Parser::isLegalVarName(str)) {
			Type* varValue = Parser::getVariableValue(str);
			if (varValue != nullptr)
				return varValue;
			else throw NameErrorException(str);
		}
		

		Type* res = Parser::getType(str);
		if (res != nullptr) {
			res->setIsTemp(true);
			return res;
		}
		//else
		// check if it is an assigment
		if (Parser::makeAssignment(str)) {
			return new Void();
		}
		throw SyntaxException();
	}

	return NULL;
}

Type* Parser::getType(std::string& str)
{
	if (Helper::isInteger(str))
		return new Integer(str);
	if (Helper::isBoolean(str))
		return new Boolean(str);
	if (Helper::isString(str))
		return new String(str);
	if (Helper::isList(str))
		std::cout << "list!" << std::endl;
	return nullptr;
}

bool Parser::isLegalVarName(const std::string& str)
{
	if (str.length() == 0)
		return false;
	char c = str[0];
	if (Helper::isDigit(c) || (!Helper::isLetter(c) && !Helper::isUnderscore(c)))
		return false;
	for (int i = 1; i < str.length(); i++) {
		c = str[i];
		if (!Helper::isDigit(c) && !Helper::isLetter(c) && !Helper::isUnderscore(c))
			return false;
	}
	return true;
}

bool Parser::makeAssignment(const std::string& str)
{
	if (std::count(str.begin(), str.end(), '=') != 1 ||
		str[0] == '=' || str[str.length() - 1] == '=')
		return false;
	std::string name = str.substr(0, str.find('=', 1));
	std::string value = str.substr(str.find('=', 1)+1);
	Helper::rtrim(name);
	Helper::trim(value);
	if (!Parser::isLegalVarName(name))
		throw NameErrorException(name);
	if (Parser::isLegalVarName(value)) {
		if (Parser::_variables.find(value) == Parser::_variables.end())
			throw NameErrorException(value);
		std::string second = Parser::_variables[value]->toString();
		Parser::_variables[name] = Parser::getType(second);
		Parser::_variables[name]->setIsTemp(false);
		return new Void();
	}
	Type* valueT = Parser::getType(value);
	if (valueT == nullptr)
		throw SyntaxException();
	valueT->setIsTemp(false);
	Parser::_variables.insert({ name, valueT });
	Parser::_variables[name] = valueT;
	return true;
}

Type* Parser::getVariableValue(const std::string& str)
{
	if (Parser::_variables.find(str) != Parser::_variables.end())
		return Parser::_variables[str];
	return nullptr;
}
